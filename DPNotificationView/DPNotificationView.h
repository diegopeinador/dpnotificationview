//
//  DPNotificationView.h
//

#import <UIKit/UIKit.h>

static CGFloat const kDPNotificationViewHeight = 44.0f;
static CGFloat const kDPNotificationViewSymbolViewSidelength = 44.0f;
static NSTimeInterval const kDPNotificationViewDefaultShowDuration = 2.0;

typedef enum {
    DPNotificationViewStyleSuccess,
    DPNotificationViewStyleError,
} DPNotificationViewStyle;

@interface DPNotificationView : UIView

#pragma mark + quick presentation

+ (void)showWithStyle:(DPNotificationViewStyle)style
              message:(NSString*)message;

+ (void)showWithTintColor:(UIColor*)tintColor
                    image:(UIImage*)image
                  message:(NSString*)message
                 duration:(NSTimeInterval)duration;

#pragma mark + creators

+ (DPNotificationView*)notificationViewWithTintColor:(UIColor*)tintColor
                                               image:(UIImage*)image
                                             message:(NSString*)message;


#pragma mark - presentation

- (void)setVisible:(BOOL)showing animated:(BOOL)animated completion:(void (^)())completion;
- (void)dismissWithStyle:(DPNotificationViewStyle)style message:(NSString*)message duration:(NSTimeInterval)duration animated:(BOOL)animated;
@property (readonly, nonatomic, getter = isShowing) BOOL visible;

#pragma mark - visible properties

/**
 The image property should be used for setting the image displayed in imageView
 Only the alpha value will be used and then be tinted to a 'legible' color
 */
@property (nonatomic, strong) UIImage* image;

@property (nonatomic, strong) UIColor* tintColor;

@property (nonatomic, getter = isShowingActivity) BOOL showingActivity;

@end
