//
//  DPNotificationView.m
//

#import "DPNotificationView.h"

static NSInteger const kDPNotificationViewEmptySymbolViewTag = 666;

@interface DPNotificationView (){
    CGAffineTransform _transformToUse;
    UIView *_messageView;
    UIView *_backgroundColoredView;
    UIDynamicAnimator *_animator;
    UIImageView *_blurImageBackground;
}

#pragma mark - presentation
@property (nonatomic, weak) UIView* parentView;
@property (nonatomic, getter = isVisible) BOOL visible;

#pragma mark - content views
@property (nonatomic, strong, readonly) UIView* symbolView; // is updated by -(void)updateSymbolView
@property (nonatomic, strong) UILabel* textLabel;
@property (nonatomic, strong) UIColor* contentColor;

@end

@implementation DPNotificationView

#pragma mark + quick presentation

+ (void)showWithTintColor:(UIColor*)tintColor
                    image:(UIImage*)image
                  message:(NSString*)message
                 duration:(NSTimeInterval)duration
{
    NSAssert(message, @"'message' must not be nil.");
    
    __block DPNotificationView* note = [[DPNotificationView alloc] init];
    note.tintColor = tintColor;
    note.image = image;
    note.textLabel.text = message;
    
    void (^completion)() = ^{[note setVisible:NO animated:YES completion:nil];};
    [note setVisible:YES animated:YES completion:^{
        double delayInSeconds = duration;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            completion();
        });
    }];
    
}

+ (void)showWithStyle:(DPNotificationViewStyle)style
              message:(NSString *)message
{
    
    
    [DPNotificationView showWithTintColor:[DPNotificationView blurTintColorForStyle:style]
                                    image:[DPNotificationView imageForStyle:style]
                                  message:message
                                 duration:kDPNotificationViewDefaultShowDuration];
}

#pragma mark + creators

+ (DPNotificationView*)notificationViewWithTintColor:(UIColor*)tintColor
                                               image:(UIImage*)image
                                             message:(NSString*)message
{
    DPNotificationView* note = [[DPNotificationView alloc] init];
    note.tintColor = tintColor;
    note.image = image;
    note.textLabel.text = message;
    
    return note;
}

#pragma mark - lifecycle

- (instancetype)init
{
    self = [super initWithFrame:CGRectZero];
    if (self) {
        
        _transformToUse = CGAffineTransformIdentity;
        
        //Parent view: the keyWindow
        _parentView = [UIApplication sharedApplication].keyWindow;
        
        CGRect frame = [self bigFrame];
        self.transform = _transformToUse;
        self.frame = frame;
        [_parentView addSubview:self];
        _messageView = [[UIView alloc] init];
        [self addSubview:_messageView];
        
        _messageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
        [_messageView setBackgroundColor:[UIColor clearColor]];
        [self setBackgroundColor:[UIColor clearColor]];
        
        //Content views
        {
            //textLabel
            {
                _textLabel = [[UILabel alloc] init];
                
                UIFontDescriptor* textLabelFontDescriptor = [UIFontDescriptor preferredFontDescriptorWithTextStyle:UIFontTextStyleBody];
                _textLabel.font = [UIFont fontWithDescriptor:textLabelFontDescriptor size:17.0f];
                _textLabel.minimumScaleFactor = 0.6;
                _textLabel.lineBreakMode = NSLineBreakByTruncatingTail;
                _textLabel.adjustsFontSizeToFitWidth = YES;
                
                _textLabel.numberOfLines = 2;
                _textLabel.textColor = [UIColor whiteColor];
                _textLabel.translatesAutoresizingMaskIntoConstraints = NO;
                [_messageView addSubview:_textLabel];
            }
            //symbolView
            {
                [self updateSymbolView];
            }
        }
        
        self.autoresizingMask = UIViewAutoresizingNone;
    }
    return self;
}

#pragma mark - layout

- (void)updateConstraints
{
    [_messageView removeConstraints:_messageView.constraints];
    
    CGFloat symbolViewWidth = self.symbolView.tag != kDPNotificationViewEmptySymbolViewTag ?
                                kDPNotificationViewSymbolViewSidelength : 0.0f;
    CGFloat symbolViewHeight = kDPNotificationViewSymbolViewSidelength;
    
    NSDictionary* metrics =
        @{@"symbolViewWidth": [NSNumber numberWithFloat:symbolViewWidth],
          @"symbolViewHeight":[NSNumber numberWithFloat:symbolViewHeight]};
    
    [_messageView addConstraints:[NSLayoutConstraint
        constraintsWithVisualFormat:@"H:|-(4)-[_symbolView(symbolViewWidth)]-(5)-[_textLabel]-(10)-|"
                            options:0
                            metrics:metrics
                              views:NSDictionaryOfVariableBindings(_textLabel, _symbolView)]];
    
    [_messageView addConstraints:[NSLayoutConstraint
        constraintsWithVisualFormat:@"V:[_symbolView(symbolViewHeight)]"
                            options:0
                            metrics:metrics
                                views:NSDictionaryOfVariableBindings(_symbolView)]];
    
    CGFloat topInset = 14;
    
    [_messageView addConstraint:[NSLayoutConstraint
                constraintWithItem:_symbolView
                         attribute:NSLayoutAttributeTop
                         relatedBy:NSLayoutRelationEqual
                            toItem:self
                         attribute:NSLayoutAttributeTop
                         multiplier:0.0f constant:topInset]];
    
    [_messageView addConstraint:[NSLayoutConstraint
        constraintWithItem:_textLabel
                 attribute:NSLayoutAttributeCenterY
                 relatedBy:NSLayoutRelationEqual
                    toItem:_symbolView
                 attribute:NSLayoutAttributeCenterY
                multiplier:1.0f constant:0]];
    
    [super updateConstraints];
}

#pragma mark - tint color

- (void)setTintColor:(UIColor *)tintColor
{
    _tintColor = tintColor;

    if (!_backgroundColoredView) {
        _backgroundColoredView = [[UIView alloc] initWithFrame:_messageView.bounds];
        _backgroundColoredView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        _backgroundColoredView.opaque = NO;
        [_messageView addSubview:_backgroundColoredView];
        [_messageView sendSubviewToBack:_backgroundColoredView];
    }
    _backgroundColoredView.backgroundColor=[tintColor colorWithAlphaComponent:0.8];
    self.contentColor = [self legibleTextColorForBlurTintColor:tintColor];
}

#pragma mark - presentation

- (void)setVisible:(BOOL)visible animated:(BOOL)animated completion:(void (^)())completion
{
    if (_visible != visible) {
        
        if (visible) {
            _messageView.frame = [self hiddenFrame];
            [self updateBackground];
            
            UIDynamicAnimator *animator = [[UIDynamicAnimator alloc] initWithReferenceView:self];
            
            UIGravityBehavior *gravityBehavior = [[UIGravityBehavior alloc] initWithItems:@[_messageView]];
            
            UICollisionBehavior *collisionBehavior = [[UICollisionBehavior alloc] initWithItems:@[_messageView]];
            collisionBehavior.translatesReferenceBoundsIntoBoundary = YES;
            
            UIDynamicItemBehavior *dynamicItem = [[UIDynamicItemBehavior alloc] initWithItems:@[_messageView]];
            dynamicItem.elasticity = 0.5;
            dynamicItem.allowsRotation = NO;
            
            [animator addBehavior:dynamicItem];
            [animator addBehavior:collisionBehavior];
            [animator addBehavior:gravityBehavior];
            
            _animator = animator;
            
            if (completion) {
                double delayInSeconds = 0.5;
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                    completion();
                });
            }
        }else{
            [_blurImageBackground removeFromSuperview];
            [UIView animateWithDuration:0.4 animations:^{
                _messageView.frame = [self hiddenFrame];
            }completion:^(BOOL finished) {
                [self removeFromSuperview];
                if (completion) {
                    completion();
                }
            }];
        }

        _visible = visible;
    } else if (completion) {
        completion();
    }
}

-(void)updateBackground{
    
    CGRect frame = CGRectIntersection(_parentView.frame, self.frame);
    
    CGRect frameTransformed = CGRectIntersection(CGRectApplyAffineTransform(_parentView.frame, _transformToUse), CGRectApplyAffineTransform(self.frame, _transformToUse));
    
    UIGraphicsBeginImageContextWithOptions(frameTransformed.size, YES, 2.0);
    CGContextRef currentConext = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(currentConext, _parentView.frame.size.height/2, _parentView.frame.size.width/2);
    CGContextConcatCTM(currentConext, CGAffineTransformInvert(_transformToUse));
    CGRect transformedParentFrame=CGRectApplyAffineTransform(_parentView.frame, _transformToUse);
    CGContextTranslateCTM(currentConext, -transformedParentFrame.size.height/2, -transformedParentFrame.size.width/2);
    if (_transformToUse.d==-1) {
        CGContextTranslateCTM(currentConext, 400, -256);
    }else{
        CGContextTranslateCTM(currentConext, -1*_transformToUse.d*(frame.origin.x), _transformToUse.c*(frame.origin.y));
    }

    [_parentView.layer renderInContext:currentConext];
    UIImage *imageToBlur = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    UIImage* imgBlur=[self blurImage:imageToBlur radius:5.0];
    
    if (!_blurImageBackground) {
        _blurImageBackground = [[UIImageView alloc] initWithFrame:_messageView.bounds];
        [_messageView addSubview:_blurImageBackground];
        [_messageView sendSubviewToBack:_blurImageBackground];
    }
    _blurImageBackground.image = imgBlur;
    
    _blurImageBackground.alpha = 0;
    [UIView animateWithDuration:0.5 delay:0.5 options:0 animations:^{
        _blurImageBackground.alpha = 1;
    } completion:nil ];
}

- (void)dismissWithStyle:(DPNotificationViewStyle)style message:(NSString *)message duration:(NSTimeInterval)duration animated:(BOOL)animated
{
    NSParameterAssert(message);

    __block typeof(self) weakself = self;
    [UIView animateWithDuration:0.1 animations:^{

        weakself.showingActivity = NO;
        weakself.image = [DPNotificationView imageForStyle:style];
        weakself.textLabel.text = message;
        weakself.tintColor = [DPNotificationView blurTintColorForStyle:style];
        
    } completion:^(BOOL finished) {
        double delayInSeconds = duration;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [weakself setVisible:NO animated:animated completion:nil];
        });
    }];
}

#pragma mark - frame calculation

- (CGFloat)topLayoutGuideLengthCalculation
{
    CGFloat top = CGRectGetHeight([[UIApplication sharedApplication] statusBarFrame]);
    
    if ( UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation) ) {
        top = CGRectGetWidth([[UIApplication sharedApplication] statusBarFrame]);
    }
    
    return top;
}

- (CGFloat)widthLayoutGuideLengthCalculation
{
    CGFloat width = CGRectGetWidth(_parentView.frame);
    
    if ( UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation) ) {
        width = CGRectGetHeight(_parentView.frame);
    }
    
    return MIN(width, 480);
}

- (CGRect)hiddenFrame
{
    CGRect offscreenFrame = self.bounds;
    offscreenFrame.size.height /= 2;
    return offscreenFrame;
}

- (CGRect)bigFrame
{
    CGFloat topLayoutGuideLength = [self topLayoutGuideLengthCalculation];
    
    CGFloat width = [self widthLayoutGuideLengthCalculation];
    
    CGRect offscreenFrame = CGRectZero;
    switch ([UIApplication sharedApplication].statusBarOrientation) {
        case UIInterfaceOrientationLandscapeLeft:
            offscreenFrame = CGRectMake( -(kDPNotificationViewHeight + topLayoutGuideLength),
                                        (CGRectGetHeight(_parentView.frame) - width)/2,
                                        (kDPNotificationViewHeight + topLayoutGuideLength)*2, width);
            _transformToUse = CGAffineTransformMakeRotation(-M_PI_2);
            break;
        case UIInterfaceOrientationLandscapeRight:
            offscreenFrame = CGRectMake(CGRectGetWidth(_parentView.frame) - (kDPNotificationViewHeight + topLayoutGuideLength),
                                        (CGRectGetHeight(_parentView.frame) - width)/2,
                                        (kDPNotificationViewHeight + topLayoutGuideLength)*2, width);
            _transformToUse = CGAffineTransformMakeRotation(M_PI_2);
            break;
        case UIInterfaceOrientationPortraitUpsideDown:
            offscreenFrame = CGRectMake((CGRectGetWidth(_parentView.frame) - width)/2,
                                        CGRectGetHeight(_parentView.frame)-(kDPNotificationViewHeight + topLayoutGuideLength), width,
                                        (kDPNotificationViewHeight + topLayoutGuideLength)*2);
            _transformToUse = CGAffineTransformMakeRotation(M_PI);
            break;
            
        default: // and UIInterfaceOrientationPortrait
            offscreenFrame = CGRectMake((CGRectGetWidth(_parentView.frame) - width)/2,
                                        -kDPNotificationViewHeight - topLayoutGuideLength, width,
                                        (kDPNotificationViewHeight + topLayoutGuideLength)*2);
            _transformToUse = CGAffineTransformIdentity;
            break;
    }

    return offscreenFrame;
}

- (CGSize)intrinsicContentSize
{
    return self.frame.size;
}

#pragma mark - symbol view

- (void)updateSymbolView
{
    [self.symbolView removeFromSuperview];
    
    if (self.isShowingActivity) {
        UIActivityIndicatorView* indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        indicator.color = self.contentColor;
        [indicator startAnimating];
        _symbolView = indicator;
    } else if (self.image) {
        //Generate UIImageView for symbolView
        UIImageView* imageView = [[UIImageView alloc] init];
        imageView.opaque = NO;
        imageView.backgroundColor = [UIColor clearColor];
        imageView.translatesAutoresizingMaskIntoConstraints = NO;
        imageView.contentMode = UIViewContentModeCenter;
        imageView.image = [self imageFromAlphaChannelOfImage:self.image replacementColor:self.contentColor];
        _symbolView = imageView;
    } else {
        _symbolView = [[UIView alloc] initWithFrame:CGRectZero];
        _symbolView.tag = kDPNotificationViewEmptySymbolViewTag;
    }
    _symbolView.translatesAutoresizingMaskIntoConstraints = NO;
    [_messageView addSubview:_symbolView];
    [self setNeedsUpdateConstraints];

}

#pragma mark -- image

- (void)setImage:(UIImage *)image
{
    if (![_image isEqual:image]) {
        _image = image;
        [self updateSymbolView];
    }
}

#pragma mark -- activity

- (void)setShowingActivity:(BOOL)showingActivity
{
    if (_showingActivity != showingActivity) {
        _showingActivity = showingActivity;
        [self updateSymbolView];
    }
}


#pragma mark - content color

- (void)setContentColor:(UIColor *)contentColor
{
    if (![_contentColor isEqual:contentColor]) {
        _contentColor = contentColor;
        self.textLabel.textColor = _contentColor;
        [self updateSymbolView];
    }
}

#pragma mark helpers

- (UIImage*) blurImage:(UIImage*)image radius:(CGFloat)radius
{
    // create our blurred image
    CIContext *context = [CIContext contextWithOptions:nil];
    CIImage *inputImage = [CIImage imageWithCGImage:image.CGImage];
    
    // setting up Gaussian Blur (we could use one of many filters offered by Core Image)
    CIFilter *filter = [CIFilter filterWithName:@"CIGaussianBlur"];
    [filter setValue:inputImage forKey:kCIInputImageKey];
    [filter setValue:[NSNumber numberWithFloat:radius] forKey:@"inputRadius"];
    CIImage *result = [filter valueForKey:kCIOutputImageKey];
    
    // CIGaussianBlur has a tendency to shrink the image a little,
    // this ensures it matches up exactly to the bounds of our original image
    CGImageRef cgImage = [context createCGImage:result fromRect:[inputImage extent]];
    
    UIImage *returnImage = [UIImage imageWithCGImage:cgImage];//create a UIImage for this function to "return" so that ARC can manage the memory of the blur... ARC can't manage CGImageRefs so we need to release it before this function "returns" and ends.
    CGImageRelease(cgImage);//release CGImageRef because ARC doesn't manage this on its own.
    
    return returnImage;
}

- (UIColor*)legibleTextColorForBlurTintColor:(UIColor*)blurTintColor
{
    CGFloat r, g, b, a;
    BOOL couldConvert = [blurTintColor getRed:&r green:&g blue:&b alpha:&a];
    
    UIColor* textColor = [UIColor whiteColor];
    
    CGFloat average = (r+g+b)/3.0; //Not considering alpha here, transperency is added by toolbar
    if (couldConvert && average > 0.65) //0.65 is mostly gut-feeling
    {
        textColor = [[UIColor alloc] initWithWhite:0.2 alpha:1.0];
    }
    
    return textColor;
}

- (UIImage*)imageFromAlphaChannelOfImage:(UIImage*)image replacementColor:(UIColor*)tintColor
{
    if (!image) return nil;
    NSParameterAssert([tintColor isKindOfClass:[UIColor class]]);
 
    //Credits: https://gist.github.com/omz/1102091
    CGRect rect = CGRectMake(0, 0, image.size.width, image.size.height);
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, image.scale);
    CGContextRef c = UIGraphicsGetCurrentContext();
    [image drawInRect:rect];
    CGContextSetFillColorWithColor(c, [tintColor CGColor]);
    CGContextSetBlendMode(c, kCGBlendModeSourceAtop);
    CGContextFillRect(c, rect);
    UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return result;
}

+ (UIImage*)imageForStyle:(DPNotificationViewStyle)style
{
    UIImage* matchedImage = nil;
    switch (style) {
        case DPNotificationViewStyleSuccess:
            matchedImage = [UIImage imageNamed:@"DPNotificationView_checkmarkIcon"];
            break;
        case DPNotificationViewStyleError:
            matchedImage = [UIImage imageNamed:@"DPNotificationView_exclamationMarkIcon"];
            break;
        default:
            break;
    }
    return matchedImage;
}

+ (UIColor*)blurTintColorForStyle:(DPNotificationViewStyle)style
{
    UIColor* blurTintColor;
    switch (style) {
        case DPNotificationViewStyleSuccess:
            blurTintColor = [UIColor colorWithRed:0.21 green:0.72 blue:0.00 alpha:1.0];
            break;
        case DPNotificationViewStyleError:
            blurTintColor = [UIColor redColor];
            break;
        default:
            break;
    }
    return blurTintColor;
}

@end
