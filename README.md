#DPNotificationView

Easy to use, iOS-7-style, semi-translucent notification view with blur that drops from the top of the window (with physics).
Also supports displaying progress. No `viewcontroller` needed, attached to the `keyWindow`.

Forked from [https://github.com/problame/CSNotificationView](https://github.com/problame/CSNotificationView)

**Requires iOS 7.**

![screenshot](https://bitbucket.org/diegopeinador/dpnotificationview/raw/master/screenshot.png)

##Example code

###Fire up quickly

```objc
    [DPNotificationView showWithStyle:DPNotificationViewStyleError
                              message:@"A critical error happened."];
									  
    [DPNotificationView showWithStyle:DPNotificationViewStyleSuccess
                              message:@"Great, it works."];
									  
									  
```

###Activity

```objc
DPNotificationView* note = (...);
note.showingActivity = YES;

[note setVisible:YES animated:YES completion:nil];
(...)
[note dismissWithStyle:DPNotificationViewStyleSuccess message:@"Success!"
	      duration:kDPNotificationViewDefaultShowDuration animated:YES];
```

###Customize appearance

####Use your custom image

```objc
note.image = [UIImage imageNamed:@"mustache"];
```

####Flexible with text & no images

```objc
    [DPNotificationView showWithTintColor:[UIColor colorWithRed:0.000 green:0.6 blue:1.000 alpha:1]
                                    image:nil
                                  message:@"No icon and a message that needs two rows and extra presentation time to be displayed properly."
                                 duration:5.8f];

```
