//
//  DPAppDelegate.h
//

#import <UIKit/UIKit.h>

@interface DPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
