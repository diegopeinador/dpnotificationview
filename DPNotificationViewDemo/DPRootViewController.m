//
//  DPRootViewController.m
//

#import "DPRootViewController.h"
#import "DPNotificationView.h"

@interface DPRootViewController ()

@property (nonatomic, strong) DPNotificationView* permanentNotification;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *progressButton;

@end

@implementation DPRootViewController

- (IBAction)showError:(id)sender {
    [DPNotificationView showWithStyle:DPNotificationViewStyleError
                              message:@"A critical error happened."];
}
- (IBAction)showSuccess:(id)sender {
    [DPNotificationView showWithStyle:DPNotificationViewStyleSuccess
                              message:@"Great, it works."];
}

- (IBAction)showCustom:(id)sender {
    [DPNotificationView showWithTintColor:[UIColor colorWithRed:0.000 green:0.6 blue:1.000 alpha:1]
                                    image:nil
                                  message:@"No icon and a message that needs two rows and extra presentation time to be displayed properly."
                                 duration:5.8f];
    
}

- (IBAction)showPermanent:(id)sender
{
    if (self.permanentNotification) {
        [self cancel];
        return;
    }else{
        [_progressButton setTitle:@"Cancel"];
    }
    
    self.permanentNotification =
        [DPNotificationView notificationViewWithTintColor:[UIColor colorWithRed:0.000 green:0.6 blue:1.000 alpha:1]
                                                    image:nil message:@"I am running for two seconds."];
    
    [self.permanentNotification setShowingActivity:YES];
    
    __block typeof(self) weakself = self;
    [self.permanentNotification setVisible:YES animated:YES completion:^{
        
        double delayInSeconds = 2.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [weakself success];
        });
        
    }];
}

- (void)cancel
{
    [_progressButton setTitle:@"Progress"];
    [self.permanentNotification dismissWithStyle:DPNotificationViewStyleError
                                         message:@"Cancelled"
                                        duration:kDPNotificationViewDefaultShowDuration animated:YES];
    self.permanentNotification = nil;
    
}

- (void)success
{
    [_progressButton setTitle:@"Progress"];
    [self.permanentNotification dismissWithStyle:DPNotificationViewStyleSuccess
                                             message:@"Sucess!"
                                            duration:kDPNotificationViewDefaultShowDuration animated:YES];
    self.permanentNotification = nil;
}


@end
